require 'json'
require 'socket'

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

$switches = []
$weight = []
$myCarPos = -1
$lap = 0
$gt = -1
$base = 1

$tracks = ['usa', 'keimola', 'germany', 'france']
$w = [0.591 , 0.31, 0.249, 0.277]

puts "#{bot_name} connected to #{server_host}:#{server_port}"

class SuperDuper
  def initialize(server_host, server_port, bot_name, bot_key)
    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, tcp)
  end

  private

  def play(bot_name, bot_key, tcp)
    tcp.puts join_message(bot_name, bot_key)
    react_to_messages_from_server tcp
  end

  def react_to_messages_from_server(tcp)
    myCarPosition = 0, track = ''
    while json = tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']
      gameTick = message['gameTick']

      case msgType
      when 'carPositions'
        myCarPosition = 0
        msgData.each_with_index {|val, index|
          car = msgData[index]
          if car['id']['name'] == 'SuperDuper'
            piece = car['piecePosition']
            myCarPosition = piece['pieceIndex'].to_i
            $lap = piece['lap'].to_i
            break
          end
        }
        nextPosition = myCarPosition + 1
        nextNextPosition = myCarPosition + 2

        if nextPosition > $weight.length
          nextPosition = 0
          nextNextPosition = 1
        end

        currentTrack = $weight[myCarPosition]
        nextTrack = $weight[nextPosition]

        unless currentTrack.nil?
          cT = (currentTrack.to_f / 10000)
        end
        unless nextTrack.nil?
          nT = (nextTrack.to_f / 10000)
        end

        if cT <= 0
          cT = 0.01
        end

        if nT <= 0
          nT = 0.01
        end

        $base = (((cT * 0.44) + (nT * 0.44)) / 2) + $w[$tr.to_i].to_f

        if $base > 1
          $base = 1
        end

        if $base < 0.25
          $base = 0.25
        end

        lane = $switches[nextNextPosition].to_s

        if $gt != gameTick
          $gt = gameTick
          if (lane == 'Right' && gameTick) or (lane == 'Left' && gameTick)
            tcp.puts switch_lane(lane)
          elsif gameTick
            tcp.puts throttle_message($base)
          end
        end
      else
        case msgType
        when 'join'
          puts 'Joined'
        when 'gameInit'
          puts 'Game Init'
          track = msgData['race']['track']['pieces']
          trackid = msgData['race']['track']['id']
          puts trackid
          if trackid == 'usa'
            $tr = 0
            $str = 8000
          elsif trackid == 'keimola'
            $tr = 1
            $str = 7300
          elsif trackid == 'germany'
            $tr = 2
            $str = 6900
          elsif trackid == 'france'
            $tr = 3
            $str = 8100
          else
            $tr = 1
            $str = 7500
          end
          calcTrack(track)
        when 'gameStart'
          puts 'Race started'
        when 'crash'
          puts 'Someone crashed -----------------------------xxxxxx------------------'
        when 'gameEnd'
          puts 'Race ended'
        when 'error'
          puts "ERROR: #{msgData}"
        end
        puts "Got #{msgType}"
        tcp.puts ping_message
      end
    end
  end

  def calcTrack(track)
    size = 0
    camber = $str
    track.each_with_index {|val, index|
      if track[index].has_key? 'length'
        $weight[index] = $str
      elsif track[index].has_key? 'angle'
        camber = track[index]['angle'].to_f * track[index]['radius'].to_f
        size += camber
        $weight[index] = camber.abs
      end
      if track[index].has_key? 'switch'
        if size >= 0
          $switches[index] = 'Right'
        elsif size < 0
          $switches[index] = 'Left'
        end
        size = 0
      end
    }
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def join_race_message(bot_name, bot_key, track, password, count)
    make_msg("joinRace", { :botId => { :name => bot_name, :key => bot_key }, :trackName => track, :password => password, :carCount => count })
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def throttle_message_tick(throttle, gameTick)
    make_msg_tick("throttle", throttle, gameTick)
  end

  def switch_lane(dir)
    make_msg("switchLane", dir)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end

  def make_msg_tick(msgType, data, gameTick)
    JSON.generate({:msgType => msgType, :data => data, :gameTick => gameTick})
  end

end

SuperDuper.new(server_host, server_port, bot_name, bot_key)
